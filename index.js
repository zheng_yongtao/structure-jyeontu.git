const { Heap, MinHeap, MaxHeap } = require("./src/Heap/index");
const { LFUCache } = require("./src/LFUCache/index");
const { LinkLine } = require("./src/LinkLine/index");
const { PriorityQueue } = require("./src/PriorityQueue/index");
const { TrieTree } = require("./src/TrieTree/index");
module.exports = {
    Heap, MinHeap, MaxHeap,LFUCache,LinkLine,PriorityQueue,TrieTree
}