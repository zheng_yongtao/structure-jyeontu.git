const { Heap, MinHeap, MaxHeap } = require("../../src/Heap/index");
const assert = require("assert");
describe("Heap", function () {
  describe("checkMaxHeap", function () {
    it("checkOrder", function () {
      const list = [1, 3, 2, 4, 6, 3, 0, 9, 7];
      const orderList = [...list].sort((a, b) => a - b);
      const myMaxHeap = new MaxHeap(list);
      while (myMaxHeap.front() != null) {
        assert.equal(myMaxHeap.pop(), orderList.pop());
      }
    });
  });
  describe("checkMinHeap", function () {
    it("checkOrder", function () {
      const list = [1, 3, 2, 4, 6, 3, 0, 9, 7];
      const orderList = [...list].sort((a, b) => b - a);
      const myMinHeap = new MinHeap(list);
      while (myMinHeap.front() != null) {
        assert.equal(myMinHeap.pop(), orderList.pop());
      }
    });
  });
});
