const { PriorityQueue } = require("../../src/PriorityQueue/index");
const assert = require("assert");
describe("PriorityQueue", function () {
  const list = [
    {
      value: 1,
      no: 1,
    },
    {
      value: 3,
      no: 2,
    },
    {
      value: 2,
      no: 3,
    },
    {
      value: 4,
      no: 4,
    },
    {
      value: 6,
      no: 5,
    },
    {
      value: 3,
      no: 6,
    },
    {
      value: 0,
      no: 7,
    },
    {
      value: 9,
      no: 8,
    },
    {
      value: 7,
      no: 9,
    },
    {
      value: -1,
      no: 10,
    },
  ];
  describe("checkMaxPriorityQueue", function () {
    it("checkOrder", function () {
      const compareFun = (a, b) => {
        if (a == undefined || b == undefined) return false;
        return a.value < b.value;
      };
      const orderList = [...list].sort((a, b) => (a.value > b.value ? 1 : -1));
      const myPriorityQueue = new PriorityQueue(list, compareFun);
      while (myPriorityQueue.front() != null) {
        assert.equal(myPriorityQueue.pop(), orderList.pop());
      }
    });
  });
  describe("checkMinPriorityQueue", function () {
    it("checkOrder", function () {
      const compareFun = (a, b) => {
        if (a == undefined || b == undefined) return false;
        return a.value > b.value;
      };
      const orderList = [...list].sort((a, b) => (a.value < b.value ? 1 : -1));
      const myPriorityQueue = new PriorityQueue(list, compareFun);
      while (myPriorityQueue.front() != null) {
        assert.equal(myPriorityQueue.pop(), orderList.pop());
      }
    });
  });
});
