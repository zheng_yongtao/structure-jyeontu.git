const { TrieTree } = require("../../src/TrieTree/index");
const assert = require("assert");
describe("TrieTree", function () {
    const wordList = ['app','apple','blue'];
    const trieTree = new TrieTree(wordList);
    const tree = {
        "a": {
            "p": {
                "p": {
                    "isEnd": true,
                    "l": {
                        "e": {
                            "isEnd": true
                        }
                    }
                }
            }
        },
        "b": {
            "l": {
                "u": {
                    "e": {
                        "isEnd": true
                    }
                }
            }
        }
    };
  describe("checkTrieTree", function () {
    it("checkBuild", function () {
        assert.equal(Object.entries(trieTree.tree).toString(),Object.entries(tree).toString());
    });
    it("checkgetWordList", function () {
        const wordList1 = [...wordList].sort();
        const wordList2 = trieTree.getWordList().sort();
        assert.equal(Object.entries(wordList1).toString(),Object.entries(wordList2).toString());
    });
    it("checkInsert", function () {
        const wordList = ['app','apple','blue'];
        const trieTree = new TrieTree(wordList);
        trieTree.insert('append');
        wordList.push('append');
        const wordList1 = [...wordList].sort();
        const wordList2 = trieTree.getWordList().sort();
        assert.equal(Object.entries(wordList1).toString(),Object.entries(wordList2).toString());
    });
    it("checkGetWordsByPrefix", function () {
        const wordList1 = ['app','apple'].sort();
        const wordList2 = trieTree.getWordsByPrefix('app').sort();
        assert.equal(Object.entries(wordList1).toString(),Object.entries(wordList2).toString());
    });
    it("checkIsPrefix", function () {
        assert.equal(trieTree.isPrefix('app'),true);
        assert.equal(trieTree.isPrefix('appa'),false);
    });
    it("checkDelete", function () {
        assert.equal(trieTree.delete('app'),true);
        assert.equal(trieTree.delete('appa'),false);
        const wordList1 = ['apple','blue'].sort();
        const wordList2 = trieTree.getWordList().sort();
        assert.equal(Object.entries(wordList1).toString(),Object.entries(wordList2).toString());
    });
    it("checkInit", function () {
        trieTree.init(['blue','green'])
        const wordList1 = ['blue','green'].sort();
        const wordList2 = trieTree.getWordList().sort();
        assert.equal(Object.entries(wordList1).toString(),Object.entries(wordList2).toString());
    });
  });
});
