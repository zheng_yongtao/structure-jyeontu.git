const { LinkLine } = require("../../src/LinkLine/index");
const assert = require("assert");
describe("LinkLine", function () {
  describe("checkLinkLine", function () {
    const list = [1,2,3,4,5,6,7,8,9,10];
    const linkList = new LinkLine(list);
    it("checkBuild", function () {
        const wordList1 = [...list].sort();
        const wordList2 = linkList.getValList().sort();
        assert.equal(Object.entries(wordList1).toString(),Object.entries(wordList2).toString());
    });
    it("checkShift", function () {
        assert.equal(linkList.shift(),1);
        list.shift();
        const wordList1 = [...list].sort();
        const wordList2 = linkList.getValList().sort();
        assert.equal(Object.entries(wordList1).toString(),Object.entries(wordList2).toString());
    });
    it("checkPop", function () {
        assert.equal(linkList.pop(),10);
        list.pop();
        const wordList1 = [...list].sort();
        const wordList2 = linkList.getValList().sort();
        assert.equal(Object.entries(wordList1).toString(),Object.entries(wordList2).toString());
    });
    it("checkUnshift", function () {
        list.unshift(1);
        assert.equal(linkList.unshift(1),list.length);
        const wordList1 = [...list].sort();
        const wordList2 = linkList.getValList().sort();
        assert.equal(Object.entries(wordList1).toString(),Object.entries(wordList2).toString());
    });
    it("checkAppend", function () {
        list.push(10);
        assert.equal(linkList.append(10),list.length);
        const wordList1 = [...list].sort();
        const wordList2 = linkList.getValList().sort();
        assert.equal(Object.entries(wordList1).toString(),Object.entries(wordList2).toString());
    });
    it("checkGetNode", function () {
        const node = linkList.getNode(5);
        assert.equal(node.val,list[4]);
    });
    it("checkDelete", function () {
        const node = linkList.getNode(5);
        assert.equal(node.val,list[4]);
        linkList.delete(node);
        list.splice(4,1);
        const wordList1 = [...list].sort();
        const wordList2 = linkList.getValList().sort();
        assert.equal(Object.entries(wordList1).toString(),Object.entries(wordList2).toString());
    });
  });
});
