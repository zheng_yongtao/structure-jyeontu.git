const { LFUCache } = require("../../src/LFUCache/index");
const assert = require("assert");
describe("LFUCache", function () {
  it("checkLFUCache", function () {
    const cache = new LFUCache(5);
    cache.put(0,0);
    cache.put(1,1);
    cache.put(2,2);
    cache.put(3,3);
    cache.put(4,4);
    assert.equal(cache.get(1),1);
    assert.equal(cache.get(2),2);
    cache.put(5,5);
    assert.equal(cache.get(0),-1);
  });
});
