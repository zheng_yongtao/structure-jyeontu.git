const Heap = require('./Heap/index.js');
const PriorityQueue = require('./PriorityQueue/index.js');
const TrieTree = require('./TrieTree/index.js');
const LinkLine = require('./LinkLine/index.js');
const LFUCache = require('./LFUCache/index.js');