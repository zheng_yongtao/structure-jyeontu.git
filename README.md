# structure-JYeontu

#### 介绍
javascript实现各种数据结构：堆、优先队列、字典树、LFU缓存、链表……

#### 项目目录结构
┌───LICENSE

├───package-lock.json

├───package.json

├───src                     //具体实现代码

├──────src/Heap                 //堆

├──────src/PriorityQueue        //优先队列
.
.
.
.
.
.

├───test                    //测试代码目录

├──────test/Heap                //堆结构测试

├──────test/PriorityQueue       //优先队列测试
.
.
.
.
.
.

├──────test/index.js            //测试脚本入口

├───README.md

└───README.en.md

#### 使用说明

详见blog目录下的文档说明。

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request

