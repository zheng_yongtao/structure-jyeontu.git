// 小顶堆
const { Heap } = require("./Heap");
class MinHeap {
  constructor(array = []) {
    this.oHeap = new Heap(array, (a, b) => {
      return a > b;
    });
  }
  get queue() {
    return this.oHeap.queue;
  }
  front() {
    return this.oHeap.front();
  }
  pop() {
    return this.oHeap.pop();
  }
  push() {
    this.oHeap.push();
  }
}
exports.MinHeap = MinHeap;
