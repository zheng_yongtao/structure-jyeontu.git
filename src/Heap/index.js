const { Heap } = require("./Heap");
const { MaxHeap } = require("./MaxHeap");
const { MinHeap } = require("./MinHeap");

module.exports = {
  Heap,
  MaxHeap,
  MinHeap,
};
