/**
 *  优先队列
 *  @param Array array 用于初始化的数组
 *  @param Function compareFun 自定义比较函数
 *  @returns {PriorityQueue}
 *  */ 
const { Heap } = require("../Heap/index");
class PriorityQueue {
  constructor(array = [], compareFun = null) {
    this.oHeap = new Heap(array, compareFun || this.compareFun);
  }
  get queue() {
    return this.oHeap.queue;
  }
  compareFun(a, b) {
    if (a == undefined || b == undefined) return false;
    return a.value < b.value;
  }
  front() {
    return this.oHeap.front();
  }
  pop() {
    return this.oHeap.pop();
  }
  push() {
    this.oHeap.push();
  }
}

exports.PriorityQueue = PriorityQueue;
